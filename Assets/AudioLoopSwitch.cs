﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLoopSwitch : MonoBehaviour
{
    private AudioSource audioSource;
    public GameObject Ship;
    public AudioClip loopClip;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = loopClip;
            audioSource.loop = true;
            audioSource.Play();
        }

        var healthUrgency = Ship.GetComponent<HealthTracker>().HealthPercent;
        if (healthUrgency > 0.75f)
        {
            audioSource.pitch = 2f * healthUrgency - 0.5f;
        }



    }
}
