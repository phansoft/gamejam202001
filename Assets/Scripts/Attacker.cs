﻿
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Attacker : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;

    public float targetRange = 3f;
    public float rateOfFire = 1f;
    public float agroRange = 5f;

    private float lastFire = -1000f;

    public int damage = 3;

    public GameObject Target;
    private HealthTracker targetHealth;
    private HealthTracker myHealth;
    private ParticleSystem fireEffect;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        fireEffect = gameObject.GetComponentInChildren<ParticleSystem>();
        if (damage < 0)
        {
            fireEffect.GetComponent<Renderer>().material.color = Color.green;
        }
        if (damage > 0)
        {
            fireEffect.GetComponent<Renderer>().material.color = Color.red;
        }

        myHealth = GetComponent<HealthTracker>();
        LookForNewTarget();
    }

    public void Move(Vector3 destination)
    {
        if (myHealth.IsDead) return;
        Target = null;
        targetHealth = null;
        MoveToPoint(destination);
    }
    public void HealTarget(GameObject target)
    {
        if (myHealth.IsDead) return;
        if (damage < 0)
        {
            Target = target;
            targetHealth = Target.GetComponent<HealthTracker>();
        }
    }

    public void KillTarget(GameObject target)
    {
        if (myHealth.IsDead) return;
        if (damage > 0)
        {
            Target = target;
            targetHealth = Target.GetComponent<HealthTracker>();
        }
    }


    private void MoveToPoint(Vector3 point)
    {
        if (myHealth.IsDead) return;
        navMeshAgent.SetDestination(point);
    }

    //Will attempt to heal/kill the target, range has already been checked.
    private void AttemptDamage()
    {
        if (myHealth.IsDead) return;
        //check rate of fire
        if (Time.time > lastFire + rateOfFire)
        {
            // they have not fired in long enough
            lastFire = Time.time;
            if (targetHealth.TakeDamage(damage))
            {
                if (!fireEffect.isPlaying)
                {
                    fireEffect.Play();
                }
            }
            else
            {
                if (fireEffect.isPlaying)
                {
                    fireEffect.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                }
                LookForNewTarget();


            }
        }
    }

    //there current object is not able to take damage, pick a new target so find a new one
    private void LookForNewTarget()
    {
        if (myHealth.IsDead) return;
        if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
        {
            return;
        }
        var possibleTargets = FindObjectsOfType<HealthTracker>();
        var rangeToTarget = agroRange;
        HealthTracker nextTarget = null;
        foreach (var possibleTarget in possibleTargets.Where(pt => !pt.IsDead))
        {
            var possibleHealth = possibleTarget.GetComponent<HealthTracker>();
            var distance = Vector3.Distance(transform.position, possibleTarget.transform.position) - possibleHealth.OuterRange;
            if (distance < rangeToTarget)
            {
                if (
                    (possibleTarget.gameObject.layer == gameObject.layer && damage < 0) ||
                    (possibleTarget.gameObject.layer != gameObject.layer && damage > 0)
                    )
                {
                    //healing target
                    if (possibleTarget.CanTakeDamage(damage))
                    {
                        rangeToTarget = distance;
                        nextTarget = possibleTarget;
                    }
                    
                }
            }
        }

        if (nextTarget != null)
        {
            Target = nextTarget.gameObject;
            targetHealth = Target.GetComponent<HealthTracker>();
        }

    }

    private Vector3 GetPointDistanceFromObject(Vector3 source , Vector3 destination , float distanceFromTarget)
    {
        // vector pointing from the planet to the player
        Vector3 difference = source - destination;

        // the distance between the player and the planet
        float distance = difference.magnitude;

        // the direction of the launch, normalized
        Vector3 directionOnly = difference.normalized;

        // the point along this vector you are requesting
        Vector3 pointAlongDirection = destination + (directionOnly * distanceFromTarget);
        return pointAlongDirection;
    }

    // Update is called once per frame
    void Update()
    {
        if (myHealth.IsDead) return;
        if (Target != null) 
        {
            if (targetHealth.IsDead)
            {
                Target = null;
                targetHealth = null;
                return;
            }
            if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
            {
                //Don't change current move orders
                return;
            }

            var distanceFromValidTarget = Vector3.Distance(transform.position, Target.transform.position) -
                                          targetHealth.OuterRange - targetRange - navMeshAgent.stoppingDistance;
            if (distanceFromValidTarget > Mathf.Epsilon)
            {
                var attackPosition = GetPointDistanceFromObject(transform.position, Target.transform.position,
                    targetHealth.OuterRange + targetRange); 
                MoveToPoint(attackPosition); 
                if (fireEffect.isPlaying)
                {
                    fireEffect.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                }
            }
            else
            {
                AttemptDamage();

            }
        }
        else
        {
            if (fireEffect.isPlaying)
            {
                fireEffect.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            }

            LookForNewTarget();
        }

    }
}
