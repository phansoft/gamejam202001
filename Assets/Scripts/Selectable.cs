﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;


public class Selectable : MonoBehaviour
{
    private Transform selectionMarker;
    private Renderer selectionMarkerRenderer;
    public LayerMask ClickablesLayers;

    private Attacker attacker;
    //public float minDistance = 2f;

    // Start is called before the first frame update
    void Start()
    {
        selectionMarker = gameObject.transform.Find("SelectionMarker");
        selectionMarkerRenderer = selectionMarker.GetComponent<Renderer>();
        selectionMarkerRenderer.enabled = false;
        attacker = GetComponent<Attacker>();
    }

    //When the Primitive collides with the walls, it will reverse direction
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "SelectionBox")
        {
            selectionMarkerRenderer.enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "SelectionBox" && Input.GetMouseButton(0))
        {
            selectionMarkerRenderer.enabled = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
        //A new selection happening so clear this one.
        if (Input.GetMouseButtonDown(0))
        {
            selectionMarkerRenderer.enabled = false;
        }
        if (attacker)
        {
            if (Input.GetMouseButtonDown(1) && selectionMarkerRenderer.enabled)
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit raycastHit,
                    ClickablesLayers))
                {
                    var layerHit = raycastHit.collider.gameObject.layer;

                    if (layerHit == LayerMask.NameToLayer("Floor"))
                    {
                        //Move to location and wait
                        attacker.Move(raycastHit.point);
                    }

                    if (layerHit == LayerMask.NameToLayer("GoodGuys"))
                    {
                        attacker.HealTarget(raycastHit.collider.gameObject);
                    }

                    if (layerHit == LayerMask.NameToLayer("BadGuys"))
                    {
                        attacker.KillTarget(raycastHit.collider.gameObject);
                    }
                }
            }
        }
    }

   



   
}
