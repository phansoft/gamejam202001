﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{
    private HealthTracker myHealth;

    // Start is called before the first frame update
    void Start()
    {
        myHealth = GetComponent<HealthTracker>();
    }

    // Update is called once per frame
    void Update()
    {
        if (myHealth.IsDead)
        {
            var gameEndText = GameObject.Find("Game End Text");
            gameEndText.GetComponent<MeshRenderer>().enabled = true;
            var endText = gameEndText.GetComponent<TextMesh>();
            endText.color = Color.red;
            endText.text = "The ship has been destroyed. We all died.";
        }

        if (!myHealth.CanTakeDamage(-1))
        {
            var gameEndText = GameObject.Find("Game End Text");
            gameEndText.GetComponent<MeshRenderer>().enabled = true;
            var endText = gameEndText.GetComponent<TextMesh>();
            endText.color = Color.green;
            endText.text = "The ship has been repaired. We all escape.";
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}
