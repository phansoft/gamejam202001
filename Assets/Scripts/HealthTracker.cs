﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTracker : MonoBehaviour
{
    [SerializeField]
    private int hitPointsMax = 100;
    [SerializeField]
    private int hitPointsCurrent = 100;

    public float HealthPercent => (float) hitPointsCurrent / (float) hitPointsMax;

    private TextMesh heathText;

    public float OuterRange = 0f;
    public bool IsDead => hitPointsCurrent <= 0;

    // Start is called before the first frame update
    void Start()
    {
        heathText = gameObject.transform.Find("HealthText").GetComponent<TextMesh>();
        heathText.text = (hitPointsCurrent * 100 / hitPointsMax) + "%";
    }

    // // Update is called once per frame
    // void Update()
    // {
    //
    // }

    public bool TakeDamage(int amount)
    {
        if (CanTakeDamage(amount))
        {
            hitPointsCurrent -= amount;
            //did we over heal
            if (hitPointsCurrent > hitPointsMax)
            {
                hitPointsCurrent = hitPointsMax;
            }

            heathText.text = (hitPointsCurrent * 100 / hitPointsMax) + "%";
            //heathText.text = hitPointsCurrent.ToString();
            if (hitPointsCurrent <= 0)
            {

                //Give everything targeting me 2 seconds to realize it then destroy myself.
                foreach (var cr in gameObject.GetComponentsInChildren<Renderer>())
                {
                    cr.material.color = new Color(cr.material.color.r, cr.material.color.g, cr.material.color.b, 0.001f);
                }
                Invoke("Suicide",0);
                
            }
            return true;
        }

        return false;
    }

    public void Suicide()
    {
        if (name != "Ship") Destroy(gameObject);
    }

    public bool CanTakeDamage(int amount)
    {
        if (amount < 0 && hitPointsCurrent < hitPointsMax || amount > 0 && hitPointsCurrent > 0)
        {
                return true;
        }
        return false;
    }
}
