﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements.Experimental;

public class CharacterSpawnerScript : MonoBehaviour
{
    public GameObject GameUnit;

    public int InitialSpawn = 5;

    public float MaxSpawnRate = 1f;

    public float SpawnDistance = 20f;

    public GameObject Ship;
    private float LastSpawn =  -100f;


    public float Urgency
    {
        get
        {
            var timeUrgency = Time.time / 300f; // How far into 5 minutes are we 
            var healthUrgency = Ship.GetComponent<HealthTracker>().HealthPercent; // more urgent as it gets closer to done
            return Mathf.Clamp(timeUrgency + healthUrgency / 2, 0, 1);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        for (var i = 0; i < InitialSpawn; i++)
        {
            Spawn();
        }
        
    }

    public void Spawn()
    {
        var randomPosition = new Vector3(Random.Range(-1f * SpawnDistance, SpawnDistance) + transform.position.x , 0, Random.Range(-1f * SpawnDistance, SpawnDistance) + transform.position.z);
        NavMesh.SamplePosition(randomPosition, out var hit, 100f, NavMesh.AllAreas);
        var finalPosition = hit.position;
        var newChar = Instantiate(GameUnit, finalPosition, Quaternion.identity);
    }

    void Update()
    {
        if (MaxSpawnRate <= Mathf.Epsilon) return;
        
        var spawnTime = MaxSpawnRate/Urgency; //as it gets to 100% (1) urgency it will increase spawn rate.
        if (Time.time > LastSpawn + spawnTime)
        {
            LastSpawn = Time.time;
            Spawn();
        }
    }

}

public class Urgency
{
}
