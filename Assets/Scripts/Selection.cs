﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selection : MonoBehaviour
{
    private Camera mainCamera;


    public LayerMask clickablesLayer;

    private Vector3 startSelect;

    private Vector3 endSelect;


    [SerializeField]
    private float selectionHeight = 2f;

    private GameObject selectionBox;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        selectionBox = GameObject.Find("SelectionBox");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit raycastHit, clickablesLayer))
            {
                startSelect = raycastHit.point;
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit raycastHit, 1000f, clickablesLayer))
            {
                endSelect = raycastHit.point;
                var length = endSelect.x - startSelect.x;
                var width = endSelect.z - startSelect.z;
                var midpoint = new Vector3(startSelect.x + (length / 2f), selectionHeight, startSelect.z + (width / 2f));
                selectionBox.transform.position = midpoint;
                selectionBox.transform.localScale = new Vector3(Mathf.Abs(length), selectionHeight, Mathf.Abs(width));
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            selectionBox.transform.position = new Vector3(0,-10,0);
            selectionBox.transform.localScale = Vector3.one;
        }
    }
}
