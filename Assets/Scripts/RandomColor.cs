﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var r = GetComponent<Renderer>();
        r.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        
        
    }

    // void Awake()
    // {
    //     var material = GetComponent<Material>();
    //     //material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    //     material.color = Color.green;
    // }

    // Update is called once per frame
    void Update()
    {
        
    }
}
